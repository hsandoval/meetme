﻿using Microsoft.EntityFrameworkCore;
using SalesApi.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace SalesApi.Data
{
    public class SalesDbContext : DbContext
    {
        public SalesDbContext(DbContextOptions<SalesDbContext> dbContextOptions) : base(dbContextOptions) { }

        private ModelBuilder Builder { get; set; }

        public DbSet<Person> Persons { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            Builder = builder;
            RenameTables();
            SetUniqueKeys();
            SetDefaultValues();
        }

        private void RenameTables()
        {
            Builder.HasDefaultSchema("Sales");
            base.OnModelCreating(Builder);
        }

        private void SetUniqueKeys()
        {
        }

        private void SetDefaultValues()
        {
            IEnumerable<Type> entitiesClass =
                typeof(Program).GetTypeInfo().Assembly.GetTypes()
                .Where(x => x.IsClass && typeof(IDefaultValues).IsAssignableFrom(x) && !x.IsAbstract)
                .Select(x => x);

            foreach (Type entity in entitiesClass)
            {
                MethodInfo methodBuilderSetDefaults = GetType().GetMethod(nameof(SalesDbContext.BuilderSetDefaults), BindingFlags.NonPublic | BindingFlags.Instance);
                MethodInfo genericMethod = methodBuilderSetDefaults.MakeGenericMethod(entity);
                genericMethod.Invoke(this, null);
            }
        }

        private void BuilderSetDefaults<TEntity>() where TEntity : class
        {
            Builder.Entity<TEntity>(build =>
            {
                build.Property(nameof(EntityBaseAuditable.IsActive)).HasDefaultValue(true);
                build.Property(nameof(EntityBaseAuditable.IsDeleted)).HasDefaultValue(false);
            });
        }

    }
}
