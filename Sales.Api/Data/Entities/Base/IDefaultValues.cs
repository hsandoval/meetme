﻿using System;

namespace SalesApi.Data.Entities
{
    internal interface IDefaultValues
    {
        DateTime CreatedDate { get; set; }
        bool? IsActive { get; set; }
        bool? IsDeleted { get; set; }
    }
}
