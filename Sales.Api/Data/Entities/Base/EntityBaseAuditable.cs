﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SalesApi.Data.Entities
{
    public abstract class EntityBaseAuditable : IDefaultValues, IAuditable
    {
        [Required]
        [DataType(DataType.DateTime)]
        public DateTime CreatedDate { get; set; }

        [Required]
        public string CreatedBy { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime ModifiedDate { get; set; }

        [Required]
        public string ModifiedBy { get; set; }

        [Required]
        public bool? IsActive { get; set; }

        [Required]
        public bool? IsDeleted { get; set; }
    }
}
