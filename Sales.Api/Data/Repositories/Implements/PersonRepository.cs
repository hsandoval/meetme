﻿using SalesApi.Data.Entities;
using Serilog;

namespace SalesApi.Data.Repositories
{
    public class PersonRepository : Repository<Person>, IPersonRepository
    {
        public PersonRepository(SalesDbContext context, ILogger logger) : base(context, logger) { }
    }
}
