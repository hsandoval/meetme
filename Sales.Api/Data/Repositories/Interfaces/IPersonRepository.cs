﻿using SalesApi.Data.Entities;

namespace SalesApi.Data.Repositories
{
    public interface IPersonRepository : IRepository<Person>
    {
    }
}
