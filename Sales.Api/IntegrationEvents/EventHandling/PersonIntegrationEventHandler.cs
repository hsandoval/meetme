﻿using EventBus.Abstractions;
using Microsoft.Extensions.Logging;
using SalesApi.Data.Entities;
using SalesApi.Data.Repositories;
using SalesApi.IntegrationEvents.Events;
using Serilog.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalesApi.IntegrationEvents.EventHandling
{
    public class PersonIntegrationEventHandler : IIntegrationEventHandler<PersonIntegrationEvent>
    {
        private readonly ILogger<PersonIntegrationEventHandler> logger;
        private readonly IPersonRepository personRepository;

        public PersonIntegrationEventHandler(ILogger<PersonIntegrationEventHandler> logger, IPersonRepository personRepository)
        {
            this.logger = logger;
            this.personRepository = personRepository;
        }

        public async Task Handle(PersonIntegrationEvent @event)
        {
            using (LogContext.PushProperty("IntegrationEventContext", $"{@event.Id}-{Program.AppName}"))
            {
                logger.LogInformation("----- Handling integration event: {IntegrationEventId} at {AppName} - ({@IntegrationEvent})", @event.Id, Program.AppName, @event);

                Person person = new Person
                {
                    Id = @event.PersonId,
                    FirtsName = @event.FirtsName,
                    FirstSurname = @event.LastName
                };

                await personRepository.UpdateAsync(person);
            }
        }
    }
}
