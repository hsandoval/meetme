﻿using EventBus.Abstractions;
using EventBus.Events;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SalesApi;
using SalesApi.Data;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;

namespace Sales.Api.IntegrationEvents
{
    public class PersonIntegrationEventService : IPersonIntegrationEventService
    {
        private readonly IEventBus _eventBus;
        private readonly ILogger<PersonIntegrationEventService> _logger;

        public PersonIntegrationEventService(ILogger<PersonIntegrationEventService> logger, IEventBus eventBus)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _eventBus = eventBus ?? throw new ArgumentNullException(nameof(eventBus));
        }

        public async Task PublishThroughEventBusAsync(IntegrationEvent evt)
        {
            try
            {
                await SimpleAwait();
                _eventBus.Publish(evt);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "ERROR Publishing integration event: {IntegrationEventId} from {AppName} - ({@IntegrationEvent})", evt.Id, Program.AppName, evt);
            }
        }

        public async Task SimpleAwait()
        {
            await Task.Delay(1000);
        }
    }
}
