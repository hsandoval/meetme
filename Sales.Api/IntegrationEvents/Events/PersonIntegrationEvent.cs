﻿using EventBus.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalesApi.IntegrationEvents.Events
{
    public class PersonIntegrationEvent : IntegrationEvent
    {
        public int PersonId { get; set; }

        public string FirtsName { get; set; }

        public string LastName { get; set; }
    }
}
