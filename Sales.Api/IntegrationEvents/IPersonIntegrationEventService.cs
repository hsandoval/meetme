﻿using System.Threading.Tasks;
using EventBus.Events;

namespace Sales.Api.IntegrationEvents
{
    public interface IPersonIntegrationEventService
    {
        Task PublishThroughEventBusAsync(IntegrationEvent evt);
    }
}