﻿namespace SalesApi.Model
{
    public class PersonModel
    {
        public int Id { get; set; }

        public string Description { get; set; }
    }
}
