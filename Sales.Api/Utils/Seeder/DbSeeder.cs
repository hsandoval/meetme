﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using SalesApi.Data;
using SalesApi.Data.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SalesApi.Utils.Seeder
{
    public class DbSeeder
    {
        private readonly SalesDbContext dbContext;
        private readonly IHostingEnvironment hosting;
        private readonly IConfiguration configuration;
        private IDictionary<string, object> DictionaryJsonFile;

        public DbSeeder(SalesDbContext dbContext, IHostingEnvironment hosting, IConfiguration configuration)
        {
            this.dbContext = dbContext;
            this.configuration = configuration;
            this.hosting = hosting;
        }

        public async Task SeedAsync()
        {
            bool dropDatabase = Convert.ToBoolean(configuration["DropDatabase"]);

            if (dropDatabase)
                await dbContext.Database.EnsureDeletedAsync();

            await dbContext.Database.EnsureCreatedAsync();

            await FillDatabaseAsync();
        }

        private async Task FillDatabaseAsync()
        {
            bool fillDatabase = Convert.ToBoolean(configuration["FillDatabase"]);

            if (fillDatabase)
            {
                string pathJsonFileSeederDatabase = configuration["PathJsonFileSeederDatabase"];
                string filePath = Path.Combine(hosting.ContentRootPath, pathJsonFileSeederDatabase);
                string json = await File.ReadAllTextAsync(filePath);
                DictionaryJsonFile = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
            }
        }

#pragma warning disable IDE0051 // TODO: Remove unused private members
        private async Task PopulateData<TEntity>(string nameKey) where TEntity : class
#pragma warning restore IDE0051 // Remove unused private members
        {
            try
            {
                DbSet<TEntity> dbSet = dbContext.Set<TEntity>();
                if (!dbSet.Any())
                {
                    if (DictionaryJsonFile.ContainsKey(nameKey))
                    {
                        IEnumerable<TEntity> listItems = JsonConvert.DeserializeObject<IEnumerable<TEntity>>(DictionaryJsonFile[nameKey].ToString());
                        await dbSet.AddRangeAsync(listItems);
                        await dbContext.SaveChangesAsync();
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
