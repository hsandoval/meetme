﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SalesApi.Data;
using SalesApi.Utils.Seeder;

namespace SalesApi.Utils.Initializers
{
    public class SeederDbInitializer : IInitializer
    {
        public void InitializerServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient(x =>
                new DbSeeder(
                    x.GetRequiredService<SalesDbContext>(),
                    x.GetRequiredService<IHostingEnvironment>(),
                    configuration
                )
            );
        }
    }
}
