﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;

namespace SalesApi.Utils.Initializers
{
    public class LogDbInitializer : IInitializer
    {
        public void InitializerServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton<ILogger>(implement =>
                new LoggerConfiguration()
                .WriteTo
                .MSSqlServer(
                        connectionString: configuration["ConnectionStringLog"],
                        tableName: "Log",
                        restrictedToMinimumLevel: Serilog.Events.LogEventLevel.Warning,
                        autoCreateSqlTable: true
                )
                .CreateLogger()
            );
        }
    }
}
