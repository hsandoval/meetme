﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SalesApi.Data;
using SalesApi.Data.Entities;

namespace SalesApi.Utils.Initializers
{
    public class AppDbInitializer : IInitializer
    {
        public void InitializerServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<SalesDbContext>(option =>
                option.UseSqlServer(configuration["ConnectionString"]),
                contextLifetime: ServiceLifetime.Transient,
                optionsLifetime: ServiceLifetime.Scoped
            );
        }
    }
}
