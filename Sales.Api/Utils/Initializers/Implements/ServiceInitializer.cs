﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SalesApi.Services;

namespace SalesApi.Utils.Initializers
{
    public class ServiceInitializer : IInitializer
    {
        public void InitializerServices(IServiceCollection services, IConfiguration configuration) =>
            services
                .AddTransient<IPersonService, PersonService>();
    }
}
