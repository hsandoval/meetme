﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;

namespace SalesApi.Utils.Initializers
{
    public static class MainExtensionInitializer
    {
        public static void InstallServicesInAssembly(this IServiceCollection services, IConfiguration configuration)
        {
            typeof(Startup).Assembly.ExportedTypes
                .Where(x => typeof(IInitializer).IsAssignableFrom(x) && !x.IsInterface && !x.IsAbstract)
                .Select(Activator.CreateInstance)
                .Cast<IInitializer>()
                .ToList()
                .ForEach(initializer => initializer.InitializerServices(services, configuration));
        }
    }
}
