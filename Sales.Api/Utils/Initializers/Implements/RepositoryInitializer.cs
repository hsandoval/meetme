﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SalesApi.Data.Repositories;

namespace SalesApi.Utils.Initializers
{
    public class RepositoryInitializer : IInitializer
    {
        public void InitializerServices(IServiceCollection services, IConfiguration configuration)
        {
            services
                .AddTransient<IPersonRepository, PersonRepository>();
        }
    }
}
