﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace SalesApi.Utils.Initializers
{
    public class MvcInitializer : IInitializer
    {
        public void InitializerServices(IServiceCollection services, IConfiguration configuration)
        {
            services
                .AddMvc()
                .AddJsonOptions(option =>
                {
                    option.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                    option.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                })
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }
    }
}
