﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace SalesApi.Utils.Initializers
{
    public interface IInitializer
    {
        void InitializerServices(IServiceCollection services, IConfiguration configuration);
    }
}
