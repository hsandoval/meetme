﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Sales.Api.IntegrationEvents;
using Sales.Api.IntegrationEvents.Events;

namespace SalesApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private readonly IPersonIntegrationEventService personIntegrationEventService;

        public ValuesController(IPersonIntegrationEventService personIntegrationEventService)
        {
            this.personIntegrationEventService = personIntegrationEventService;
        }

        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1.Sales", "value2.Sales" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] Person data)
        {
            var priceChangedEvent = new PersonChangedIntegrationEvent(1, data.Name, data.LastName);

            // Publish through the Event Bus and mark the saved event as published
            await personIntegrationEventService.PublishThroughEventBusAsync(priceChangedEvent);
            return Ok(data);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }

    public class Person
    {
        public string Name { get; set; }

        public string LastName { get; set; }
    }
}
