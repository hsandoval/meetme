﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using Security.Api.Controllers;
using Security.Api.Model;
using Security.Api.Services;
using Serilog;
using System.Threading.Tasks;
using Xunit;
using SignInResult = Microsoft.AspNetCore.Identity.SignInResult;

namespace SecurityTest.Controllers
{
    public class AccountControllerTest
    {
        private readonly AccountController accountController;
        private readonly Mock<IAccountServices> accountServices;

        public AccountControllerTest()
        {
            Mock<ILogger> iLogger = new Mock<ILogger>();
            accountServices = new Mock<IAccountServices>();
            accountController = new AccountController(iLogger.Object, accountServices.Object);
        }

        [Fact]
        public async Task IsRequiredUserNameOrUserEmail()
        {
            AccountModel loginModel = new AccountModel
            {
                Password = "Abc123456$",
                //UserEmail = "user@hotmail.com"
            };
            accountServices.Setup(opt => opt.PasswordSignInAsync(loginModel)).ReturnsAsync(new SignInResult());
            accountController.ModelState.AddModelError("UserEmail", $"The property UserEmail is required because the property UserName has not a value.");
            accountController.ModelState.AddModelError("UserName", $"The property UserName is required because the property UserEmail has not a value.");

            IActionResult result = await accountController.SignInAsync(loginModel);

            BadRequestObjectResult badRequestResult = Assert.IsAssignableFrom<BadRequestObjectResult>(result);
        }

        [Fact]
        public async Task GetMenuStructured()
        {
            //Arrange
            //accountServices.Setup(x => x.Get)
        }
    }
}
