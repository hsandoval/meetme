﻿using HelperJsonFile;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Newtonsoft.Json;
using Security.Api.Controllers;
using Security.Api.Data.Entities;
using Security.Api.Services;
using Serilog;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace SecurityTest.Controllers
{
    public class MenuControllerTest
    {
        private readonly ReadJsonFile file;
        private Mock<IMenuServices> menuServices;
        private readonly MenuController menuController;

        public MenuControllerTest()
        {
            Mock<ILogger> iLogger = new Mock<ILogger>();
            file = new ReadJsonFile("TestData.json");
            menuServices = new Mock<IMenuServices>();
            menuController = new MenuController(iLogger.Object, menuServices.Object);
        }

        [Fact]
        public async Task GetMenu()
        {
            object data = await file.GetElementInJsonFileByKey("Applications");
            IEnumerable<Application> result = JsonConvert.DeserializeObject<IEnumerable<Application>>(data.ToString());
            menuServices.Setup(x => x.GetMenu()).ReturnsAsync(result);

            IActionResult actionResult = await menuController.GetMenuByUser();
            Assert.IsType<OkObjectResult>(actionResult);
        }
    }
}
