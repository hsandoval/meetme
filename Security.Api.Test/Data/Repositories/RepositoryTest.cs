﻿using Microsoft.EntityFrameworkCore;
using Serilog;
using Moq;
using Security.Api.Data;
using Security.Api.Data.Entities;
using Security.Api.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace SecurityTest.Data.Repositories
{
    public class RepositoryTest
    {
        private Mock<ILogger> logger;

        public RepositoryTest()
        {
            logger = new Mock<ILogger>();
        }

        [Fact]
        public async Task TestAddAsync()
        {
            #region Arrange
            Application expectedResult = BuilderApplicationElement();
            DbContextOptions<SecurityDbContext> options = new DbContextOptionsBuilder<SecurityDbContext>()
                .UseInMemoryDatabase(databaseName: nameof(TestAddAsync))
                .Options; ;
            #endregion

            #region Act
            using (SecurityDbContext context = new SecurityDbContext(options))
            {
                ApplicationRepository repository = new ApplicationRepository(context, logger.Object);
                await repository.AddAsync(expectedResult);
            }
            #endregion

            #region Assert
            using (SecurityDbContext context = new SecurityDbContext(options))
            {
                ApplicationRepository repository = new ApplicationRepository(context, logger.Object);
                int elementsInDatabase = await context.Applications.CountAsync();
                Application result = await context.Applications.SingleAsync();
                Assert.Equal(1, elementsInDatabase);
                Assert.Equal(expectedResult.Description, result.Description);
            }
            #endregion
        }

        [Fact]
        public async Task TestAddRangeAsync()
        {
            #region Arrange
            const int QUANTITY_ELEMENTS = 5;
            IEnumerable<Application> expectedResult;
            IEnumerable<Application> result;

            expectedResult = Enumerable.Range(1, QUANTITY_ELEMENTS).Select(i => BuilderApplicationElement(i)).ToList().AsEnumerable();

            DbContextOptions<SecurityDbContext> options = new DbContextOptionsBuilder<SecurityDbContext>()
                .UseInMemoryDatabase(databaseName: nameof(TestAddRangeAsync))
                .Options; ;
            #endregion

            #region Act
            using (SecurityDbContext context = new SecurityDbContext(options))
            {
                ApplicationRepository repository = new ApplicationRepository(context, logger.Object);
                result = await repository.AddAsync(expectedResult);
            }
            #endregion

            #region Assert
            using (SecurityDbContext context = new SecurityDbContext(options))
            {
                ApplicationRepository repository = new ApplicationRepository(context, logger.Object);
                int elementsInDatabase = await context.Applications.CountAsync();
                Assert.Equal(QUANTITY_ELEMENTS, elementsInDatabase);
                Assert.Equal(expectedResult, result);
            }
            #endregion
        }

        [Fact]
        public async Task TestDeleteAsync()
        {
            #region Arrange
            Application application = BuilderApplicationElement();
            DbContextOptions<SecurityDbContext> options = new DbContextOptionsBuilder<SecurityDbContext>()
                .UseInMemoryDatabase(databaseName: nameof(TestDeleteAsync))
                .Options;
            bool isSuccessfully = false;

            using (SecurityDbContext context = new SecurityDbContext(options))
            {
                ApplicationRepository repository = new ApplicationRepository(context, logger.Object);
                await repository.AddAsync(application);
            }
            #endregion

            #region Act
            using (SecurityDbContext context = new SecurityDbContext(options))
            {
                ApplicationRepository repository = new ApplicationRepository(context, logger.Object);
                isSuccessfully = await repository.RemovePhysicalAsync(new Application { Id = 1 });
            }
            #endregion

            #region Assert
            using (SecurityDbContext context = new SecurityDbContext(options))
            {
                ApplicationRepository repository = new ApplicationRepository(context, logger.Object);
                int elementsInDatabase = await context.Applications.CountAsync();
                Assert.True(isSuccessfully);
                Assert.Equal(0, elementsInDatabase);
            }
            #endregion
        }

        [Fact]
        public async Task TestDeleteRangeAsync()
        {
            #region Arrange
            const int QUANTITY_ELEMENTS = 50;
            long expectedResult = 35;
            long quantityRemaining;
            IEnumerable<Application> elementsToInsert;
            elementsToInsert = Enumerable.Range(1, QUANTITY_ELEMENTS).Select(i => BuilderApplicationElement(i)).ToList().AsEnumerable();
            IEnumerable<Application> elementsToDelete = elementsToInsert.ToList().GetRange(5, 15);
            IDictionary<Application, bool> elementsDeleted;


            DbContextOptions<SecurityDbContext> options = new DbContextOptionsBuilder<SecurityDbContext>()
                .UseInMemoryDatabase(databaseName: nameof(TestDeleteRangeAsync))
                .Options; ;

            using (SecurityDbContext context = new SecurityDbContext(options))
            {
                ApplicationRepository repository = new ApplicationRepository(context, logger.Object);
                await repository.AddAsync(elementsToInsert);
            }
            #endregion

            #region Act
            using (SecurityDbContext context = new SecurityDbContext(options))
            {
                ApplicationRepository repository = new ApplicationRepository(context, logger.Object);
                elementsDeleted = await repository.RemovePhysicalAsync(elementsToDelete);
                quantityRemaining = await repository.CountAsync();
            }
            #endregion

            #region Assert
            bool allElementsDeleted = elementsDeleted.Any(x => x.Value);
            Assert.True(allElementsDeleted);
            Assert.Equal(expectedResult, quantityRemaining);
            #endregion
        }

        [Fact]
        public async Task TestCountAsync()
        {
            #region Arrange
            const int QUANTITY_ELEMENTS = 5;
            long expectedResult = 5;
            long result;
            IEnumerable<Application> elementsToInsert;

            elementsToInsert = Enumerable.Range(1, QUANTITY_ELEMENTS).Select(i => BuilderApplicationElement(i)).ToList().AsEnumerable();

            DbContextOptions<SecurityDbContext> options = new DbContextOptionsBuilder<SecurityDbContext>()
                .UseInMemoryDatabase(databaseName: nameof(TestCountAsync))
                .Options; ;

            using (SecurityDbContext context = new SecurityDbContext(options))
            {
                ApplicationRepository repository = new ApplicationRepository(context, logger.Object);
                await repository.AddAsync(elementsToInsert);
            }
            #endregion

            #region Act
            using (SecurityDbContext context = new SecurityDbContext(options))
            {
                ApplicationRepository repository = new ApplicationRepository(context, logger.Object);
                result = await repository.CountAsync();
            }
            #endregion

            #region Assert
            Assert.Equal(expectedResult, result);
            #endregion
        }

        [Fact]
        public async Task TestFindWithParameter()
        {
            #region Arrange
            const int QUANTITY_ELEMENTS = 50;
            int expectedResult = 50;
            int result;
            IEnumerable<Application> elementsToInsert;
            elementsToInsert = Enumerable.Range(1, QUANTITY_ELEMENTS).Select(i => BuilderApplicationElement(i)).ToList().AsEnumerable();

            DbContextOptions<SecurityDbContext> options = new DbContextOptionsBuilder<SecurityDbContext>()
                .UseInMemoryDatabase(databaseName: nameof(TestFindWithParameter))
                .Options; ;

            using (SecurityDbContext context = new SecurityDbContext(options))
            {
                ApplicationRepository repository = new ApplicationRepository(context, logger.Object);
                await repository.AddAsync(elementsToInsert);
            }
            #endregion

            #region Act
            using (SecurityDbContext context = new SecurityDbContext(options))
            {
                ApplicationRepository repository = new ApplicationRepository(context, logger.Object);
                result = (await repository.Find()).Count();
            }
            #endregion

            #region Assert
            Assert.Equal(expectedResult, result);
            #endregion
        }

        [Fact]
        public async Task TestFindWithoutParameter()
        {
            #region Arrange
            const int QUANTITY_ELEMENTS = 50;
            int expectedResult = 5;
            int result;
            IEnumerable<Application> elementsToInsert;
            elementsToInsert = Enumerable.Range(1, QUANTITY_ELEMENTS).Select(i => BuilderApplicationElement(i)).ToList().AsEnumerable();

            DbContextOptions<SecurityDbContext> options = new DbContextOptionsBuilder<SecurityDbContext>()
                .UseInMemoryDatabase(databaseName: nameof(TestFindWithoutParameter))
                .Options; ;

            using (SecurityDbContext context = new SecurityDbContext(options))
            {
                ApplicationRepository repository = new ApplicationRepository(context, logger.Object);
                await repository.AddAsync(elementsToInsert);
            }
            #endregion

            #region Act
            using (SecurityDbContext context = new SecurityDbContext(options))
            {
                ApplicationRepository repository = new ApplicationRepository(context, logger.Object);
                result = (await repository.Find(x => x.Description.Contains("0"))).Count();
            }
            #endregion

            #region Assert
            Assert.Equal(expectedResult, result);
            #endregion
        }

        [Fact]
        public async Task TestGetAll()
        {
            #region Arrange
            const int QUANTITY_ELEMENTS = 50;
            int expectedResult = 50;
            int result;
            IEnumerable<Application> elementsToInsert;
            elementsToInsert = Enumerable.Range(1, QUANTITY_ELEMENTS).Select(i => BuilderApplicationElement(i)).ToList().AsEnumerable();

            DbContextOptions<SecurityDbContext> options = new DbContextOptionsBuilder<SecurityDbContext>()
                .UseInMemoryDatabase(databaseName: nameof(TestGetAll))
                .Options; ;

            using (SecurityDbContext context = new SecurityDbContext(options))
            {
                ApplicationRepository repository = new ApplicationRepository(context, logger.Object);
                await repository.AddAsync(elementsToInsert);
            }
            #endregion

            #region Act
            using (SecurityDbContext context = new SecurityDbContext(options))
            {
                ApplicationRepository repository = new ApplicationRepository(context, logger.Object);
                result = (repository.GetAll()).Count();
            }
            #endregion

            #region Assert
            Assert.Equal(expectedResult, result);
            #endregion
        }

        [Fact]
        public async Task TestFindByIdAsync()
        {
            #region Arrange
            const int QUANTITY_ELEMENTS = 20;
            IEnumerable<Application> elementsToInsert = Enumerable
                .Range(1, QUANTITY_ELEMENTS)
                .Select(i => BuilderApplicationElement(i))
                .ToList()
                .AsEnumerable();
            Application result;

            DbContextOptions<SecurityDbContext> options = new DbContextOptionsBuilder<SecurityDbContext>()
                .UseInMemoryDatabase(databaseName: nameof(TestFindByIdAsync))
                .Options; ;

            using (SecurityDbContext context = new SecurityDbContext(options))
            {
                ApplicationRepository repository = new ApplicationRepository(context, logger.Object);
                await repository.AddAsync(elementsToInsert);
            }
            #endregion

            #region Act
            using (SecurityDbContext context = new SecurityDbContext(options))
            {
                ApplicationRepository repository = new ApplicationRepository(context, logger.Object);
                result = await repository.FindByIdAsync(4);
            }
            #endregion

            #region Assert
            Application expectedResult = elementsToInsert.Single(x => x.Id == 4);
            Assert.Equal(expectedResult.Description, result.Description);
            #endregion
        }

        [Fact]
        public async Task TestUpdateAsync()
        {
            #region Arrange
            IEnumerable<Application> elementsToInsert;
            Application recordUpdate;
            elementsToInsert = Enumerable.Range(1, 20).Select(i => BuilderApplicationElement(i)).ToList().AsEnumerable();

            DbContextOptions<SecurityDbContext> options = new DbContextOptionsBuilder<SecurityDbContext>()
                .UseInMemoryDatabase(databaseName: nameof(TestUpdateAsync))
                .Options; ;

            using (SecurityDbContext context = new SecurityDbContext(options))
            {
                ApplicationRepository repository = new ApplicationRepository(context, logger.Object);
                await repository.AddAsync(elementsToInsert);
            }
            #endregion

            #region Act
            using (SecurityDbContext context = new SecurityDbContext(options))
            {
                ApplicationRepository repository = new ApplicationRepository(context, logger.Object);
                recordUpdate = elementsToInsert.First();
                recordUpdate.Description = "Element modified";
                recordUpdate = await repository.UpdateAsync(recordUpdate);
            }
            #endregion

            #region Assert
            using (SecurityDbContext context = new SecurityDbContext(options))
            {
                ApplicationRepository repository = new ApplicationRepository(context, logger.Object);
                Application result = await context.Applications.FindAsync(1);
                Assert.Equal(recordUpdate.Description, result.Description);
            }
            #endregion
        }

        [Fact]
        public async Task TestUpdateRangeAsync()
        {
            #region Arrange
            IEnumerable<Application> elementsToInsert;
            IEnumerable<Application> recordUpdate;
            IEnumerable<Application> elementsResult;
            elementsToInsert = Enumerable.Range(1, 20).Select(i => BuilderApplicationElement(i)).ToList().AsEnumerable();

            DbContextOptions<SecurityDbContext> options = new DbContextOptionsBuilder<SecurityDbContext>()
                .UseInMemoryDatabase(databaseName: nameof(TestUpdateRangeAsync))
                .Options; ;

            using (SecurityDbContext context = new SecurityDbContext(options))
            {
                ApplicationRepository repository = new ApplicationRepository(context, logger.Object);
                await repository.AddAsync(elementsToInsert);
            }
            #endregion

            #region Act
            using (SecurityDbContext context = new SecurityDbContext(options))
            {
                ApplicationRepository repository = new ApplicationRepository(context, logger.Object);
                recordUpdate = elementsToInsert.Select(x =>
                {
                    x.Description = string.Format("{0} {1}", x.Description, DateTime.Now.ToShortTimeString());
                    return x;
                });

                elementsResult = await repository.UpdateAsync(recordUpdate);
            }
            #endregion

            #region Assert
            using (SecurityDbContext context = new SecurityDbContext(options))
            {
                ApplicationRepository repository = new ApplicationRepository(context, logger.Object);
                Application result = await context.Applications.FindAsync(1);
                //Assert.Equal(recordUpdate.Description, result.Description);
            }
            #endregion
        }

        private Application BuilderApplicationElement(int id = 0)
        {
            string stringIdentifier;
            int identifier;

            identifier = id != 0 ? id : 0;
            stringIdentifier = identifier == 0 ? string.Empty : string.Format("_{0}", identifier);

            return new Application
            {
                Id = identifier,
                Description = $"Application{stringIdentifier}",
                IsActive = true,
                IsDeleted = false,
                CreatedBy = "test",
                CreatedDate = DateTime.Now,
                ModifiedBy = "test",
                ModifiedDate = DateTime.Now
            };
        }
    }
}
