﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace HelperJsonFile
{
    public class ReadJsonFile
    {
        private readonly string jsonFileName;
        private readonly StreamReader streamReader;

        public ReadJsonFile(string pathFile)
        {
            jsonFileName = pathFile;
            streamReader = new StreamReader(pathFile);
        }

        public async Task<object> GetElementInJsonFileByKey(string key)
        {
            Dictionary<string, object> data = await ReadFile();
            if (data.ContainsKey(key))
                return data[key];

            throw new KeyNotFoundException(string.Format("The key:{0}, not exists in json file:{1} ", key, jsonFileName));
        }

        private async Task<Dictionary<string, object>> ReadFile()
        {
            string json = await streamReader.ReadToEndAsync();
            return JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
        }
    }
}
