﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace GenericRepository
{
    interface IRepository<TEntity> where TEntity : class
    {
        Task<IEnumerable<TEntity>> AddAsync(IEnumerable<TEntity> entities);
        Task<TEntity> AddAsync(TEntity entity);
        Task<long> CountAsync();
        Task<IQueryable<TEntity>> Find(Expression<Func<TEntity, bool>> predicate);
        Task<TEntity> FindByIdAsync(int id);
        IQueryable<TEntity> GetAll();
        Task<IDictionary<TEntity, bool>> RemoveAsync(IEnumerable<TEntity> entities);
        Task<bool> RemoveAsync(TEntity entity);
        Task<IEnumerable<TEntity>> UpdateAsync(IEnumerable<TEntity> entities);
        Task<TEntity> UpdateAsync(TEntity entity);
    }
}
