﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace GenericRepository
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private readonly DbContext Context;
        private readonly DbSet<TEntity> DbSet;
        private readonly ILogger<Repository<TEntity>> Logger;

        public Repository(IDbContext context, ILogger<Repository<TEntity>> logger, DbContext dbContext, DbContextOptionsBuilder dbContextOptions)
        {
            Context = dbContextOptions.Options;
            DbSet = Context.Set<TEntity>();
            Logger = logger;
        }

        public async Task<TEntity> AddAsync(TEntity entity)
        {
            try
            {
                DbSet.Add(entity);
                await Context.SaveChangesAsync();
                return entity;
            }
            catch (Exception ex)
            {
                Logger.LogWarning($"StackTrace: {ex.StackTrace} \n Message: {ex.Message}");
                throw new Exception(ex.StackTrace);
            }
        }

        public async Task<IEnumerable<TEntity>> AddAsync(IEnumerable<TEntity> entities)
        {
            try
            {
                await Context.AddRangeAsync(entities);
                await Context.SaveChangesAsync();
                return entities;
            }
            catch (Exception ex)
            {
                Logger.LogWarning($"StackTrace: {ex.StackTrace} \n Message: {ex.Message}");
                throw new Exception(ex.StackTrace);
            }
        }

        public async Task<long> CountAsync()
        {
            try
            {
                return await DbSet.LongCountAsync();
            }
            catch (Exception ex)
            {
                Logger.LogWarning($"{ex.StackTrace}");
                throw new Exception(ex.StackTrace);
            }
        }

        public Task<IQueryable<TEntity>> Find(Expression<Func<TEntity, bool>> predicate)
        {
            if (predicate == null)
                return Task.FromResult(DbSet.AsQueryable());

            return Task.FromResult(DbSet.Where(predicate).AsQueryable());
        }

        public async Task<TEntity> FindByIdAsync(int id)
        {
            try
            {
                return await DbSet.FindAsync(id);
            }
            catch (Exception ex)
            {
                Logger.LogWarning($"StackTrace: {ex.StackTrace} \n Message: {ex.Message}");
                throw new Exception(ex.StackTrace);
            }
        }

        public IQueryable<TEntity> GetAll()
        {
            return DbSet.AsQueryable();
        }

        public async Task<bool> RemoveAsync(TEntity entity)
        {
            try
            {
                DbSet.Remove(entity);
                return (await Context.SaveChangesAsync()) > 0;
            }
            catch (Exception ex)
            {
                Logger.LogWarning($"{ex.StackTrace}");
                throw new Exception(ex.StackTrace);
            }
        }

        public async Task<IDictionary<TEntity, bool>> RemoveAsync(IEnumerable<TEntity> entities)
        {
            try
            {
                IDictionary<TEntity, bool> result = new Dictionary<TEntity, bool>();
                List<Task> tasks = entities.Select(async x => result.Add(x, await RemoveAsync(x))).ToList();
                await Task.WhenAll(tasks);
                return result;
            }
            catch (Exception ex)
            {
                Logger.LogWarning($"{ex.StackTrace}");
                throw new Exception(ex.StackTrace);
            }
        }

        public async Task<TEntity> UpdateAsync(TEntity entity)
        {
            try
            {
                Context.Entry(entity).State = EntityState.Modified;
                await Context.SaveChangesAsync();
                return entity;
            }
            catch (Exception ex)
            {
                Logger.LogWarning($"StackTrace: {ex.StackTrace} \n Message: {ex.Message}");
                throw new Exception(ex.StackTrace);
            }
        }

        public async Task<IEnumerable<TEntity>> UpdateAsync(IEnumerable<TEntity> entities)
        {
            try
            {
                List<Task<TEntity>> tasks = entities.Select(async x => await UpdateAsync(x)).ToList();
                await Task.WhenAll(tasks);
                return entities;
            }
            catch (Exception ex)
            {
                Logger.LogWarning($"{ex.StackTrace}");
                throw new Exception(ex.StackTrace);
            }
        }
    }
}
