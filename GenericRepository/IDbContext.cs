﻿using Microsoft.EntityFrameworkCore;

namespace GenericRepository
{
    public interface IDbContext 
    {
        DbSet<T> Set<T>() where T : class;
    }
}
