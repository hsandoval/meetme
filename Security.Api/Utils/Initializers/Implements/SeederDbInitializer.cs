﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Security.Api.Data;
using Security.Api.Data.Entities;
using Security.Api.Utils.Seeder;

namespace Security.Api.Utils.Initializers
{
    public class SeederDbInitializer : IInitializer
    {
        public void InitializerServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient(x =>
                new DbSeeder(
                    x.GetRequiredService<SecurityDbContext>(),
                    x.GetRequiredService<RoleManager<Role>>(),
                    x.GetRequiredService<UserManager<User>>(),
                    x.GetRequiredService<IHostingEnvironment>(),
                    configuration
                )
            );
        }
    }
}
