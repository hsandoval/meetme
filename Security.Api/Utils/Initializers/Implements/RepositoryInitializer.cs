﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Security.Api.Data.Repositories;

namespace Security.Api.Utils.Initializers
{
    public class RepositoryInitializer : IInitializer
    {
        public void InitializerServices(IServiceCollection services, IConfiguration configuration)
        {
            services
                .AddTransient<IApplicationRepository, ApplicationRepository>()
                .AddTransient<IPersonRepository, PersonRepository>()
                .AddTransient<IIdentityRepository, IdentityRepository>()
                .AddTransient<IMenuRepository, MenuRepository>();
        }
    }
}
