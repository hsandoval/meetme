﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using Serilog.Events;

namespace Security.Api.Utils.Initializers
{
    public class LogDbInitializer : IInitializer
    {
        public void InitializerServices(IServiceCollection services, IConfiguration configuration)
        {
            string connectionString = configuration["ConnectionStringLog"] ?? configuration.GetConnectionString("LogConnection");

            services.AddSingleton<ILogger>(implement =>
                new LoggerConfiguration()
                .WriteTo
                .MSSqlServer(
                    connectionString: connectionString,
                    tableName: "Log",
                    restrictedToMinimumLevel: LogEventLevel.Warning,
                    autoCreateSqlTable: true
                )
                .CreateLogger()
            );
        }
    }
}
