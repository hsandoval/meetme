﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Security.Api.Data;
using Security.Api.Data.Entities;

namespace Security.Api.Utils.Initializers
{
    public class AppDbInitializer : IInitializer
    {
        public void InitializerServices(IServiceCollection services, IConfiguration configuration)
        {
            string connectionString = configuration["ConnectionString"] ?? configuration.GetConnectionString("DefaultConnection");

            services.AddDbContext<SecurityDbContext>(option =>
                option.UseSqlServer(connectionString),
                contextLifetime: ServiceLifetime.Transient,
                optionsLifetime: ServiceLifetime.Scoped
            );

            services.AddIdentity<User, Role>()
                .AddEntityFrameworkStores<SecurityDbContext>();
        }
    }
}
