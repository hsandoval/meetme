﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Security.Api.Services;

namespace Security.Api.Utils.Initializers
{
    public class ServiceInitializer : IInitializer
    {
        public void InitializerServices(IServiceCollection services, IConfiguration configuration) => 
            services
                .AddTransient<IAccountServices, AccountServices>()
                .AddTransient<IMenuServices, MenuService>();
    }
}
