﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;

namespace Security.Api.Utils.Initializers
{
    public class SwaggerInitializer : IInitializer
    {
        public void InitializerServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddSwaggerGen(setUp =>
            {
                setUp.SwaggerDoc("v1", new Info
                {
                    Title = "MeetMe Security API",
                    Version = "v1",
                    License = new License
                    {
                        Name = "MIT License",
                        Url = "https://gitlab.com/hsandoval/meetme/blob/master/LICENSE"
                    },
                    Contact = new Contact
                    {
                        Name = "Henk Sandoval",
                        Email = "henkalexander.sandoval@gmail.com",
                        Url = "https://gitlab.com/hsandoval"
                    },
                    TermsOfService = $@"www.google.com",
                });
            });
        }
    }
}
