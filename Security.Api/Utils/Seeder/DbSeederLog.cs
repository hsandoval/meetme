﻿using Serilog;
using System.Threading.Tasks;

namespace Security.Api.Utils.Seeder
{
    public class DbSeederLog
    {
        private readonly ILogger logger;

        public DbSeederLog(ILogger logger)
        {
            this.logger = logger;
        }

        public async Task Execute()
        {
            logger.Information("Generate database");
            await Task.Yield();
        }
    }
}
