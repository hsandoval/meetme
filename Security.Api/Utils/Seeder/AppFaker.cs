﻿using Bogus;
using Security.Api.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Security.Api.Utils.Seeder
{
    public static class AppFaker
    {
        public static IEnumerable<User> GetSampleUsersData()
        {
            Random random = new Random();
            int quantity = 10;

            Faker<UserRole> fakerUserRoles = new Faker<UserRole>()
                .RuleFor(p => p.IsActive, f => f.PickRandomParam(new bool[] { true, true, true, false }))
                .RuleFor(p => p.IsDeleted, false)
                .RuleFor(r => r.RoleId, f => f.Random.Byte(1, 3));                

            Faker<Data.Entities.Person> personFaker = new Faker<Data.Entities.Person>()
                .RuleFor(p => p.IsActive, f => f.PickRandomParam(new bool[] { true, true, true, false }))
                .RuleFor(p => p.IsDeleted, false)
                .RuleFor(p => p.PersonalIdentificationNumber, f => f.Random.AlphaNumeric(30))
                .RuleFor(p => p.FirtsName, f => f.Person.FirstName)
                .RuleFor(p => p.MiddleName, f => f.Person.FirstName)
                .RuleFor(p => p.FirstSurname, f => f.Person.LastName)
                .RuleFor(p => p.SecondSurname, f => f.Person.LastName)
                .RuleFor(p => p.CountryFK, f => f.Random.Byte(1, 243))
                .RuleFor(p => p.User, (f, p) =>
                {
                    return new User
                    {
                        IsActive = p.IsActive,
                        IsDeleted = p.IsDeleted,
                        Email = f.Internet.Email(p.FirtsName, p.FirstSurname),
                        EmailConfirmed = true,
                        PhoneNumber = f.Person.Phone,
                        PhoneNumberConfirmed = true,
                        LockoutEnabled = false,
                        UserName = $"{p.FirtsName}_{p.FirstSurname}",
                        UserRoles = fakerUserRoles.Generate(1)
                    };
                });

            return personFaker.Generate(quantity).Select(p =>
            {
                return new User
                {
                    IsActive = p.IsActive,
                    IsDeleted = p.IsDeleted,
                    Email = p.User.Email,
                    EmailConfirmed = p.User.EmailConfirmed,
                    PhoneNumber = p.User.PhoneNumber,
                    PhoneNumberConfirmed = p.User.PhoneNumberConfirmed,
                    LockoutEnabled = p.User.LockoutEnabled,
                    UserName = p.User.UserName,
                    UserRoles = p.User.UserRoles,
                    Person = new Data.Entities.Person
                    {
                        IsActive = p.IsActive,
                        IsDeleted = p.IsDeleted,
                        PersonalIdentificationNumber = p.PersonalIdentificationNumber,
                        FirtsName = p.FirtsName,
                        MiddleName = p.MiddleName,
                        FirstSurname = p.FirstSurname,
                        SecondSurname = p.SecondSurname,
                        CountryFK = p.CountryFK
                    }
                };
            });
        }
    }
}