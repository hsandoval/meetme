﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Security.Api.Data;
using Security.Api.Data.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Security.Api.Utils.Seeder
{
    public class DbSeeder
    {
        private readonly SecurityDbContext dbContext;
        private readonly IHostingEnvironment hosting;
        private readonly IConfiguration configuration;
        private readonly RoleManager<Role> roleManager;
        private readonly UserManager<User> userManager;

        private IDictionary<string, object> DictionaryJsonFile;
        private bool dropDatabase;
        private bool fillDatabase;
        private string pathJsonFileSeederDatabase;

        public DbSeeder(SecurityDbContext dbContext, RoleManager<Role> roleManager, UserManager<User> userManager, IHostingEnvironment hosting, IConfiguration configuration)
        {
            this.dbContext = dbContext;
            this.configuration = configuration;
            this.roleManager = roleManager;
            this.userManager = userManager;
            this.hosting = hosting;
        }

        public async Task Execute()
        {
            dropDatabase = Convert.ToBoolean(configuration["DropDatabase"]);
            fillDatabase = Convert.ToBoolean(configuration["FillDatabase"]);
            pathJsonFileSeederDatabase = configuration["PathJsonFileSeederDatabase"];

            await SeedAsync();
        }

        private async Task SeedAsync()
        {
            if (dropDatabase)
                await dbContext.Database.EnsureDeletedAsync();

            await dbContext.Database.EnsureCreatedAsync();

            await FillDatabaseAsync();
        }

        private async Task FillDatabaseAsync()
        {
            if (fillDatabase)
            {
                string filePath = Path.Combine(hosting.ContentRootPath, pathJsonFileSeederDatabase);
                string json = await File.ReadAllTextAsync(filePath);
                DictionaryJsonFile = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                await PopulateData<ActionType>("ActionsTypes");
                await PopulateData<Country>("Countries");
                await PopulateApplications("Applications");
                await PopulateRoles("Roles");
                await PopulateUsers("Users");
            }
        }

        private async Task PopulateData<TEntity>(string nameKey) where TEntity : class
        {
            try
            {
                DbSet<TEntity> dbSet = dbContext.Set<TEntity>();
                if (!dbSet.Any())
                {
                    if (DictionaryJsonFile.ContainsKey(nameKey))
                    {
                        IEnumerable<TEntity> listItems = JsonConvert.DeserializeObject<IEnumerable<TEntity>>(DictionaryJsonFile[nameKey].ToString());
                        await dbSet.AddRangeAsync(listItems);
                        await dbContext.SaveChangesAsync();
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private async Task PopulateRoles(string nameKey)
        {
            if (!dbContext.Roles.Any())
            {
                if (DictionaryJsonFile.ContainsKey(nameKey))
                {
                    IEnumerable<Role> roles = JsonConvert.DeserializeObject<IEnumerable<Role>>(DictionaryJsonFile[nameKey].ToString());

                    foreach (Role rol in roles)
                    {
                        await roleManager.CreateAsync(rol);
                    }
                }
            }
        }

        private async Task PopulateUsers(string nameKey)
        {
            IEnumerable<User> listUsers = AppFaker.GetSampleUsersData();
            IEnumerable<UserRole> usersRoles = new List<UserRole>();

            if (!dbContext.Persons.Any())
            {
                try
                {
                    List<Role> roles = dbContext.Roles.ToList();
                    foreach (User user in listUsers)
                    {
                        await userManager.CreateAsync(user, "Abc123456$");
                        IEnumerable<UserRole> d = user.UserRoles.Select(x =>
                            {
                                x.UserId = user.Id;
                                return x;
                            });
                        usersRoles = usersRoles.Concat(d);
                    }

                    await dbContext.UserRoles.AddRangeAsync(usersRoles);
                    await dbContext.SaveChangesAsync();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        private async Task PopulateApplications(string nameKey)
        {
            if (!dbContext.Applications.Any())
            {
                if (DictionaryJsonFile.ContainsKey(nameKey))
                {
                    IEnumerable<Application> applications = JsonConvert.DeserializeObject<IEnumerable<Application>>(DictionaryJsonFile[nameKey].ToString());

                    await dbContext.Applications.AddRangeAsync(applications);
                    await dbContext.SaveChangesAsync();
                }
            }
        }
    }
}
