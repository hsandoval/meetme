﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Security.Api
{
    public class EnvironmentConfig
    {
        public string DB_SECURITY_API { get; set; }

        public string DB_SECURITY_API_LOG { get; set; }
    }
}
