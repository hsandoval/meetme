﻿using System.ComponentModel.DataAnnotations;
using ValidationAttributeExtend;

namespace Security.Api.Model
{
    public class AccountModel
    {
        [RequiredIf(nameof(UserEmail))]
        public string UserName { get; set; }

        [RequiredIf(nameof(UserName))]
        [EmailAddress]
        public string UserEmail { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "The field {0} is required.")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public bool RememberMe { get; set; }
    }
}
