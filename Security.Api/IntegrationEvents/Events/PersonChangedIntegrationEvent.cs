﻿using EventBus.Events;

namespace Security.Api.IntegrationEvents
{
    public class PersonChangedIntegrationEvent : IntegrationEvent
    {
        public int PersonId { get; set; }

        public string FirtsName { get; set; }

        public string LastName { get; set; }

        public PersonChangedIntegrationEvent(int personId, string firtsName, string lastName)
        {
            PersonId = personId;
            FirtsName = firtsName;
            LastName = lastName;
        }
    }
}
