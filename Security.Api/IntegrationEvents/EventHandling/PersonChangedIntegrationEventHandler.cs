﻿using EventBus.Abstractions;
using Microsoft.Extensions.Logging;
using Security.Api.Data.Entities;
using Security.Api.Data.Repositories;
using Serilog.Context;
using System.Threading.Tasks;

namespace Security.Api.IntegrationEvents
{
    public class PersonChangedIntegrationEventHandler : IIntegrationEventHandler<PersonChangedIntegrationEvent>
    {
        private readonly ILogger<PersonChangedIntegrationEventHandler> logger;
        private readonly IPersonRepository personRepository;

        public PersonChangedIntegrationEventHandler(ILogger<PersonChangedIntegrationEventHandler> logger, IPersonRepository personRepository)
        {
            this.logger = logger;
            this.personRepository = personRepository;
        }

        public async Task Handle(PersonChangedIntegrationEvent @event)
        {
            using (LogContext.PushProperty("IntegrationEventContext", $"{@event.Id}-{Program.AppName}"))
            {
                logger.LogInformation("----- Handling integration event: {IntegrationEventId} at {AppName} - ({@IntegrationEvent})", @event.Id, Program.AppName, @event);

                Person person = new Person
                {
                    Id = @event.PersonId,
                    FirtsName = @event.FirtsName,
                    FirstSurname = @event.LastName
                };

                await personRepository.UpdateAsync(person);
            }
        }
    }
}
