﻿using System;

namespace Security.Api.Data.Entities
{
    interface IDefaultValues
    {
        DateTime CreatedDate { get; set; }
        bool? IsActive { get; set; }
        bool? IsDeleted { get; set; }
    }
}