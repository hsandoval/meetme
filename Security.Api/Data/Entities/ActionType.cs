﻿using System.Collections.Generic;

namespace Security.Api.Data.Entities
{
    public class ActionType : EntityBase
    {
        public virtual ICollection<Method> Methods { get; set; }

        public virtual ICollection<PermissionRole> PermissionRoles { get; set; }
    }
}
