﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Security.Api.Data.Entities
{
    public class Application : EntityBase
    {
        public virtual ICollection<Area> Areas { get; set; }
    }
}
