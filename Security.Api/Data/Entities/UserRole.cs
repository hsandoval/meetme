﻿using Microsoft.AspNetCore.Identity;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Security.Api.Data.Entities
{
    public class UserRole : IdentityUserRole<int>, IDefaultValues, IAuditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime CreatedDate { get; set; }

        public string CreatedBy { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime ModifiedDate { get; set; }

        [Required]
        public string ModifiedBy { get; set; }

        [Required]
        public bool? IsActive { get; set; }

        [Required]
        public bool? IsDeleted { get; set; }

        [NotMapped]
        public virtual User User { get; set; }

        [NotMapped]
        public virtual Role Role { get; set; }
    }
}
