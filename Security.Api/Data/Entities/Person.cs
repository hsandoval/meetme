﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Security.Api.Data.Entities
{
    public class Person : EntityBaseAuditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public int CountryFK { get; set; }

        [Required]
        [MaxLength(30)]
        public string PersonalIdentificationNumber { get; set; }

        [Required]
        [MaxLength(20)]
        public string FirtsName { get; set; }

        [MaxLength(20)]
        public string SecondSurname { get; set; }

        [MaxLength(20)]
        public string MiddleName { get; set; }

        [Required]
        [MaxLength(20)]
        public string FirstSurname { get; set; }

        [ForeignKey("CountryFK")]
        public virtual Country Country { get; set; }

        [NotMapped]
        public virtual User User { get; set; }
    }
}
