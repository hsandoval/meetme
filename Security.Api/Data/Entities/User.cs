﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Security.Api.Data.Entities
{
    [Table("Users", Schema = "Identity")]
    public class User : IdentityUser<int>, IDefaultValues, IAuditable
    {
        [Required]
        [DataType(DataType.DateTime)]
        public DateTime CreatedDate { get; set; }

        public string CreatedBy { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime ModifiedDate { get; set; }

        [Required]
        public string ModifiedBy { get; set; }

        [Required]
        public bool? IsActive { get; set; }

        [Required]
        public bool? IsDeleted { get; set; }

        [Required]
        public int PersonFK { get; set; }

        [ForeignKey("PersonFK")]
        public virtual Person Person { get; set; }

        [NotMapped]
        public virtual ICollection<UserRole> UserRoles { get; set; }
    }
}
