﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Security.Api.Data.Entities
{
    public class SubArea : EntityBase
    {
        public int AreaFK { get; set; }

        [Required]
        [ForeignKey("AreaFK")]
        public virtual Area Area { get; set; }

        public virtual ICollection<Controller> Controllers  { get; set; }
    }
}
