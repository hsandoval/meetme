﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Security.Api.Data.Entities
{
    public class Area : EntityBase
    {
        public int ApplicationFK { get; set; }

        [Required]
        [ForeignKey("ApplicationFK")]
        public virtual Application Application { get; set; }

        public virtual ICollection<SubArea> SubAreas { get; set; }
    }
}
