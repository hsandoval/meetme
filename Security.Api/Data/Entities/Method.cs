﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Security.Api.Data.Entities
{
    public class Method : EntityBase
    {
        public int ActionTypeFK { get; set; }

        public int ControllerFK { get; set; }

        [Required]
        [ForeignKey("ControllerFK")]
        public virtual Controller Controller { get; set; }

        [Required]
        [ForeignKey("ActionTypeFK")]
        public virtual ActionType MethodType { get; set; }
    }
}
