﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Security.Api.Data.Entities
{
    public class Controller : EntityBase
    {
        public int SubAreaFK { get; set; }

        [Required]
        [MaxLength(25)]
        public string UserInterfaceName { get; set; }

        [Required]
        public int Position { get; set; }

        [Required]
        [MaxLength(50)]
        public int UserInterfaceIcon { get; set; }

        [Required]
        [ForeignKey("SubAreaFK")]
        public virtual SubArea SubArea { get; set; }

        public virtual ICollection<Method> Methods { get; set; }
    }
}
