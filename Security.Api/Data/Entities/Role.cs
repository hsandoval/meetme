﻿using Microsoft.AspNetCore.Identity;
using System;
using System.ComponentModel.DataAnnotations;

namespace Security.Api.Data.Entities
{
    public class Role : IdentityRole<int>, IDefaultValues, IAuditable
    {
        [Required]
        [DataType(DataType.DateTime)]
        public DateTime CreatedDate { get; set; }

        public string CreatedBy { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime ModifiedDate { get; set; }

        [Required]
        public string ModifiedBy { get; set; }

        [Required]
        public bool? IsActive { get; set; }

        [Required]
        public bool? IsDeleted { get; set; }
    }
}
