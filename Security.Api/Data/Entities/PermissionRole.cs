﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Security.Api.Data.Entities
{
    public class PermissionRole : EntityBaseAuditable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public DateTime? DateFrom { get; set; }

        public DateTime? DateTo { get; set; }

        [Required]
        [ForeignKey("RoleFK")]
        public virtual Role  Role { get; set; }

        [Required]
        [ForeignKey("ControllerFK")]
        public Controller Controller { get; set; }

        [Required]
        [ForeignKey("ActionTypeFK")]
        public virtual ActionType ActionType { get; set; }
    }
}
