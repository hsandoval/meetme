﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Security.Api.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace Security.Api.Data
{
    public class SecurityDbContext : IdentityDbContext<User, Role, int, IdentityUserClaim<int>, UserRole, IdentityUserLogin<int>, IdentityRoleClaim<int>, IdentityUserToken<int>>
    {
        private ModelBuilder Builder { get; set; }

        public SecurityDbContext(DbContextOptions<SecurityDbContext> options) : base(options) { }

        public DbSet<Application> Applications { get; set; }
        public DbSet<Area> Areas { get; set; }
        public DbSet<SubArea> SubAreas { get; set; }
        public DbSet<Controller> Controllers { get; set; }
        public DbSet<Method> Methods { get; set; }
        public DbSet<ActionType> ActionsTypes { get; set; }
        public DbSet<PermissionRole> PermissionsRoles { get; set; }
        public DbSet<Person> Persons { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            Builder = builder;
            RenameTables();
            SetUniqueKeys();
            SetDefaultValues();
        }

        private void RenameTables()
        {
            Builder.HasDefaultSchema("Security");
            base.OnModelCreating(Builder);

            Builder.Entity<ActionType>().ToTable("Actions_Types");
            Builder.Entity<PermissionRole>().ToTable("Relation_Permissions_Roles");

            Builder.Entity<User>().ToTable("Users", "Identity");
            Builder.Entity<Role>().ToTable("Roles", "Identity");
            Builder.Entity<UserRole>().ToTable("Relation_Users_Roles", "Identity");
            Builder.Entity<IdentityUserClaim<int>>().ToTable("Users_Claims", "Identity");
            Builder.Entity<IdentityUserLogin<int>>().ToTable("Users_Logins", "Identity");
            Builder.Entity<IdentityUserToken<int>>().ToTable("Users_Tokens", "Identity");
            Builder.Entity<IdentityRoleClaim<int>>().ToTable("Roles_Claims", "Identity");
        }

        private void SetUniqueKeys()
        {
            Builder.Entity<Application>()
                .HasIndex(i => new
                {
                    i.Description
                })
                .IsUnique(true);

            Builder.Entity<Controller>()
                .HasIndex(i => new
                {
                    i.Description,
                    i.SubAreaFK
                })
                .IsUnique(true);

            Builder.Entity<Controller>()
                .HasIndex(i => new
                {
                    i.SubAreaFK,
                    i.Position
                })
                .IsUnique(true);

            Builder.Entity<Method>()
                .HasIndex(i => new
                {
                    i.Description,
                    i.ControllerFK
                })
                .IsUnique(true);

            Builder.Entity<ActionType>()
                .HasIndex(i => new
                {
                    i.Description
                })
                .IsUnique(true);
        }

        private void SetDefaultValues()
        {
            IEnumerable<Type> entitiesClass =
                typeof(Program).GetTypeInfo().Assembly.GetTypes()
                .Where(x => x.IsClass && typeof(IDefaultValues).IsAssignableFrom(x) && !x.IsAbstract)
                .Select(x => x);

            foreach (Type entity in entitiesClass)
            {
                MethodInfo methodBuilderSetDefaults = GetType().GetMethod(nameof(SecurityDbContext.BuilderSetDefaults), BindingFlags.NonPublic | BindingFlags.Instance);
                MethodInfo genericMethod = methodBuilderSetDefaults.MakeGenericMethod(entity);
                genericMethod.Invoke(this, null);
            }
        }

        private void BuilderSetDefaults<TEntity>() where TEntity : class
        {
            Builder.Entity<TEntity>(build =>
            {
                build.Property(nameof(EntityBaseAuditable.IsActive)).HasDefaultValue(true);
                build.Property(nameof(EntityBaseAuditable.IsDeleted)).HasDefaultValue(false);
            });
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            SetValuesTrackChanges();
            return await base.SaveChangesAsync(cancellationToken);
        }

        public string UserProvider => "Something";//TODO: Get Current User//if (!string.IsNullOrEmpty(WindowsIdentity.GetCurrent().Name))//    return WindowsIdentity.GetCurrent().Name.Split('\\')[1];//return string.Empty;

        public Func<DateTime> TimestampProvider => () => DateTime.UtcNow;

        private void SetValuesTrackChanges()
        {
            foreach (EntityEntry entry in ChangeTracker.Entries().Where(e => e.State == EntityState.Added || e.State == EntityState.Modified))
            {
                if (entry.Entity is IAuditable)
                {
                    IAuditable auditable = entry.Entity as IAuditable;
                    string user = UserProvider;
                    DateTime time = TimestampProvider();

                    if (entry.State == EntityState.Added)
                    {
                        auditable.ModifiedBy = auditable.CreatedBy = user;
                        auditable.ModifiedDate = auditable.CreatedDate = time;
                    }
                    else
                    {
                        auditable.ModifiedBy = user;
                        auditable.ModifiedDate = time;
                    }
                }
            }
        }
    }
}
