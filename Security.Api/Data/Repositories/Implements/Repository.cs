﻿using Microsoft.EntityFrameworkCore;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Security.Api.Data.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly SecurityDbContext securityContext;
        protected readonly DbSet<TEntity> dbSet;
        protected readonly ILogger seriLogger;

        public Repository(SecurityDbContext context, ILogger seriLogger)
        {
            securityContext = context;
            dbSet = context.Set<TEntity>();
            this.seriLogger = seriLogger;
        }

        public async Task<TEntity> AddAsync(TEntity entity)
        {
            try
            {
                dbSet.Add(entity);
                await securityContext.SaveChangesAsync();
                return entity;
            }
            catch (Exception ex)
            {
                seriLogger.Error(ex, $"StackTrace: {ex.StackTrace} \n Message: {ex.Message}", entity);
                throw ex;
            }
        }

        public async Task<IEnumerable<TEntity>> AddAsync(IEnumerable<TEntity> entities)
        {
            try
            {
                await securityContext.AddRangeAsync(entities);
                await securityContext.SaveChangesAsync();
                return entities;
            }
            catch (Exception ex)
            {
                seriLogger.Error(ex, $"StackTrace: {ex.StackTrace} \n Message: {ex.Message}", entities);
                throw ex;
            }
        }

        public async Task<long> CountAsync()
        {
            try
            {
                return await dbSet.LongCountAsync();
            }
            catch (Exception ex)
            {
                seriLogger.Error(ex, $"StackTrace: {ex.StackTrace} \n Message: {ex.Message}");
                throw ex;
            }
        }

        public Task<IQueryable<TEntity>> Find(Expression<Func<TEntity, bool>> predicate = null)
        {
            if (predicate == null)
                return Task.FromResult(dbSet.AsQueryable());

            return Task.FromResult(dbSet.Where(predicate).AsQueryable());
        }

        public async Task<TEntity> FindByIdAsync(int id)
        {
            try
            {
                return await dbSet.FindAsync(id);
            }
            catch (Exception ex)
            {
                seriLogger.Error(ex, $"StackTrace: {ex.StackTrace} \n Message: {ex.Message}", id);
                throw ex;
            }
        }

        public IQueryable<TEntity> GetAll()
        {
            return dbSet.AsQueryable();
        }

        public async Task<bool> RemovePhysicalAsync(TEntity entity)
        {
            try
            {
                dbSet.Remove(entity);
                return (await securityContext.SaveChangesAsync()) > 0;
            }
            catch (Exception ex)
            {
                seriLogger.Error(ex, $"StackTrace: {ex.StackTrace} \n Message: {ex.Message}", entity);
                throw ex;
            }
        }

        public async Task<IDictionary<TEntity, bool>> RemovePhysicalAsync(IEnumerable<TEntity> entities)
        {
            try
            {
                IDictionary<TEntity, bool> result = new Dictionary<TEntity, bool>();
                List<Task> tasks = entities.Select(async x => result.Add(x, await RemovePhysicalAsync(x))).ToList();
                await Task.WhenAll(tasks);
                return result;
            }
            catch (Exception ex)
            {
                seriLogger.Error(ex, $"StackTrace: {ex.StackTrace} \n Message: {ex.Message}", entities);
                throw ex;
            }
        }

        public async Task<TEntity> UpdateAsync(TEntity entity)
        {
            try
            {
                securityContext.Entry(entity).State = EntityState.Modified;
                await securityContext.SaveChangesAsync();
                return entity;
            }
            catch (Exception ex)
            {
                seriLogger.Error(ex, $"StackTrace: {ex.StackTrace} \n Message: {ex.Message}", entity);
                throw ex;
            }
        }

        public async Task<IEnumerable<TEntity>> UpdateAsync(IEnumerable<TEntity> entities)
        {
            try
            {
                List<Task<TEntity>> tasks = entities.Select(async x => await UpdateAsync(x)).ToList();
                await Task.WhenAll(tasks);
                return entities;
            }
            catch (Exception ex)
            {
                seriLogger.Error(ex, $"StackTrace: {ex.StackTrace} \n Message: {ex.Message}", entities);
                throw ex;
            }
        }
    }
}
