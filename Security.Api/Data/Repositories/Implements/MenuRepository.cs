﻿using Microsoft.EntityFrameworkCore;
using Security.Api.Data.Entities;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Security.Api.Data.Repositories
{
    public class MenuRepository : Repository<Application>, IMenuRepository
    {
        public MenuRepository(SecurityDbContext context, ILogger logger) : base(context, logger) { }

        public async Task<IQueryable<Application>> GetMenu()
        {
            try
            {
                IQueryable<Application> query =
                    securityContext.Applications
                        .Include(x => x.Areas)
                        .ThenInclude(x => x.SubAreas)
                        .ThenInclude(x => x.Controllers)
                        .ThenInclude(x => x.Methods);

                return await Task.FromResult(query);
            }
            catch (Exception ex)
            {
                seriLogger.Error(ex, $"StackTrace: {ex.StackTrace} \n Message: {ex.Message}");
                throw;
            }
        }
    }
}
