﻿using Security.Api.Data.Entities;
using Serilog;

namespace Security.Api.Data.Repositories
{
    public class ApplicationRepository : Repository<Application>, IApplicationRepository
    {
        public ApplicationRepository(SecurityDbContext context, ILogger logger) : base(context, logger) { }
    }
}
