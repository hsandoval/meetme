﻿using Security.Api.Data.Entities;
using Serilog;

namespace Security.Api.Data.Repositories
{
    public class PersonRepository : Repository<Person>, IPersonRepository
    {
        public PersonRepository(SecurityDbContext context, ILogger logger) : base(context, logger) { }
    }
}
