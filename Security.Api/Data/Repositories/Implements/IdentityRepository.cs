﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Security.Api.Data.Entities;
using Security.Api.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Security.Api.Data.Repositories
{
    public class IdentityRepository : IIdentityRepository
    {
        private readonly UserManager<User> userManager;
        private readonly SignInManager<User> signInManager;
        private readonly RoleManager<Role> rolesManager;
        private readonly ILogger<Repository<IdentityRepository>> logger;

        public IdentityRepository(UserManager<User> userManager, SignInManager<User> signInManager,
            RoleManager<Role> rolesManager, ILogger<Repository<IdentityRepository>> logger)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.rolesManager = rolesManager;
            this.logger = logger;
        }

        public async Task<SignInResult> PasswordSignInAsync(AccountModel model)
        {
            try
            {
                //var signedUser = await userManager.FindByEmailAsync(model.UserName);
                //var result = await signInManager.PasswordSignInAsync(model.UserName, model.Password, true, false);

                return await signInManager.PasswordSignInAsync(model.UserName, model.Password, isPersistent: false, lockoutOnFailure: true);
            }
            catch (Exception ex)
            {
                logger.LogWarning($"error:{ex.StackTrace}");
                throw new Exception(ex.Message);
            }
        }
    }
}
