﻿using Security.Api.Data.Entities;

namespace Security.Api.Data.Repositories
{
    public interface IApplicationRepository : IRepository<Application> { }
}
