﻿using Security.Api.Data.Entities;
using System.Linq;
using System.Threading.Tasks;

namespace Security.Api.Data.Repositories
{
    public interface IMenuRepository : IRepository<Application>
    {
        Task<IQueryable<Application>> GetMenu();
    }
}
