﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Security.Api.Model;

namespace Security.Api.Data.Repositories
{
    public interface IIdentityRepository
    {
        Task<SignInResult> PasswordSignInAsync(AccountModel model);
    }
}