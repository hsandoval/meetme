﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Security.Api.Data.Repositories
{
    public interface IRepository<TEntity> where TEntity : class
    {
        Task<IEnumerable<TEntity>> AddAsync(IEnumerable<TEntity> entities);
        Task<TEntity> AddAsync(TEntity entity);
        Task<long> CountAsync();
        Task<IQueryable<TEntity>> Find(Expression<Func<TEntity, bool>> predicate);
        Task<TEntity> FindByIdAsync(int id);
        IQueryable<TEntity> GetAll();
        Task<IDictionary<TEntity, bool>> RemovePhysicalAsync(IEnumerable<TEntity> entities);
        Task<bool> RemovePhysicalAsync(TEntity entity);
        Task<IEnumerable<TEntity>> UpdateAsync(IEnumerable<TEntity> entities);
        Task<TEntity> UpdateAsync(TEntity entity);
    }
}
