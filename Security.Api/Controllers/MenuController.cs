﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Security.Api.Data.Entities;
using Security.Api.Services;
using Serilog;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Security.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MenuController : Controller
    {
        private readonly IMenuServices menuServices;

        public MenuController(ILogger logger, IMenuServices menuServices) : base(logger)
        {
            this.menuServices = menuServices;
        }

        [HttpGet]
        public ActionResult<IEnumerable<string>> Get() => new string[] { "value1", "value2" };

        [HttpPost]
        [AllowAnonymous]
        [Route("GetMenuByUser")]
        public async Task<IActionResult> GetMenuByUser()
        {
            try
            {
                IEnumerable<Application> data = await menuServices.GetMenu();
                return Ok(data);
            }
            catch (Exception ex)
            {
                seriLogger.Error(ex, $"StackTrace: {ex.StackTrace} \n Message: {ex.Message}");
                throw;
            }
        }

        [HttpPut]
        [AllowAnonymous]
        [Route("UpdateMenu")]
        public async Task<IActionResult> UpdateMenu()
        {
            try
            {
                IEnumerable<Application> data = await menuServices.UpdateMenu();
                return Ok(data);
            }
            catch (Exception ex)
            {
                seriLogger.Error(ex, $"StackTrace: {ex.StackTrace} \n Message: {ex.Message}");
                throw;
            }
        }
    }
}
