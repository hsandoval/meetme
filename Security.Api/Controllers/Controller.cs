﻿using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace Security.Api.Controllers
{
    public abstract class Controller : ControllerBase
    {
        protected readonly ILogger seriLogger;

        public Controller(ILogger seriLogger)
        {
            this.seriLogger = seriLogger;
        }
    }
}
