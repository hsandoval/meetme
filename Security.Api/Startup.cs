﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using EventBus;
using EventBus.Abstractions;
using EventBus.EventBusRabbitMQ;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using Security.Api.IntegrationEvents;
using Security.Api.Utils.Initializers;
using Security.Api.Utils.Seeder;
using Security.Api.Utils.Swagger;
using Serilog;
using System;

namespace Security.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            //services.InstallServicesInAssembly(Configuration);

            //services.AddSingleton<IRabbitMQPersistentConnection>(implementationFactory =>
            //{
            //    var logger = implementationFactory.GetRequiredService<ILogger<DefaultRabbitMQPersistentConnection>>();

            //    var factory = new ConnectionFactory()
            //    {
            //        HostName = Configuration["EventBusConnection"],
            //        UserName = Configuration["EventBusUserName"],
            //        Password = Configuration["EventBusPassword"],
            //        DispatchConsumersAsync = true
            //    };

            //    var retryCount = 5;
            //    return new DefaultRabbitMQPersistentConnection(factory, logger, retryCount);
            //});

            //RegisterEventBus(services);

            //var container = new ContainerBuilder();
            //container.Populate(services);

            //return new AutofacServiceProvider(container.Build());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env/*, DbSeeder dbSeeder, ILoggerFactory loggerFactory*/)
        {
            //loggerFactory.AddFile("Logs/Log-{Date}.txt");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                //dbSeeder.Execute().GetAwaiter().GetResult();
            }

            //AppSwaggerOptions appSwaggerOptions = new AppSwaggerOptions();
            //Configuration.GetSection(nameof(AppSwaggerOptions)).Bind(appSwaggerOptions);

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            //app.UseSwagger();
            //app.UseSwaggerUI(opt =>
            //{
            //    opt.SwaggerEndpoint(appSwaggerOptions.UIEndpoint, appSwaggerOptions.Description);
            //});

            //ConfigureEventBus(app);
        }

        private void RegisterEventBus(IServiceCollection services)
        {
            var subscriptionClientName = Configuration["SubscriptionClientName"];

            services.AddSingleton<IEventBus, EventBusRabbitMQ>(sp =>
            {
                var rabbitMQPersistentConnection = sp.GetRequiredService<IRabbitMQPersistentConnection>();
                var iLifetimeScope = sp.GetRequiredService<ILifetimeScope>();
                var logger = sp.GetRequiredService<ILogger<EventBusRabbitMQ>>();
                var eventBusSubcriptionsManager = sp.GetRequiredService<IEventBusSubscriptionsManager>();

                var retryCount = 5;
                if (!string.IsNullOrEmpty(Configuration["EventBusRetryCount"]))
                {
                    retryCount = int.Parse(Configuration["EventBusRetryCount"]);
                }

                return new EventBusRabbitMQ(rabbitMQPersistentConnection, logger, iLifetimeScope, eventBusSubcriptionsManager, subscriptionClientName, retryCount);
            });

            services.AddSingleton<IEventBusSubscriptionsManager, InMemoryEventBusSubscriptionsManager>();

            services.AddTransient<PersonChangedIntegrationEventHandler>();
        }

        private void ConfigureEventBus(IApplicationBuilder app)
        {
            var eventBus = app.ApplicationServices.GetRequiredService<IEventBus>();

            eventBus.Subscribe<PersonChangedIntegrationEvent, PersonChangedIntegrationEventHandler>();
        }
    }
}
