﻿using Security.Api.Data.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Security.Api.Services
{
    public interface IMenuServices
    {
        Task<IEnumerable<Application>> GetMenu();

        Task<IEnumerable<Application>> UpdateMenu();
    }
}