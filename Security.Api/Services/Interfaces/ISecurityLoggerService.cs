﻿namespace Security.Api.Services
{
    public interface ISecurityLoggerService<T>
    {
        void LogInformation(string message, params object[] args);
        void LogWarning(string message, params object[] args);
    }
}
