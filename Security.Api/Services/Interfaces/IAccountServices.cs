﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Security.Api.Model;

namespace Security.Api.Services
{
    public interface IAccountServices
    {
        Task<SignInResult> PasswordSignInAsync(AccountModel model);
    }
}