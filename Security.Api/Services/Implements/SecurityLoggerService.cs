﻿using Microsoft.Extensions.Logging;

namespace Security.Api.Services
{
    public class SecurityLoggerService<T> : ISecurityLoggerService<T>
    {
        private readonly ILogger<T> _logger;

        public SecurityLoggerService(ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger<T>();
        }

        public void LogWarning(string message, params object[] args) =>
            _logger.LogWarning(message, args);

        public void LogInformation(string message, params object[] args) =>
            _logger.LogInformation(message, args);
    }
}
