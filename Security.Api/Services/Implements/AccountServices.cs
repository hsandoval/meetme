﻿using Microsoft.AspNetCore.Identity;
using Security.Api.Data.Repositories;
using Security.Api.Model;
using System.Threading.Tasks;

namespace Security.Api.Services
{
    public class AccountServices : IAccountServices
    {
        private readonly IIdentityRepository repository;

        public AccountServices(IIdentityRepository repository)
        {
            this.repository = repository;
        }

        public async Task<SignInResult> PasswordSignInAsync(AccountModel model) =>
            await repository.PasswordSignInAsync(model);
    }
}
