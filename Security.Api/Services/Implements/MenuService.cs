﻿using Security.Api.Data.Entities;
using Security.Api.Data.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Security.Api.Services
{
    public class MenuService : IMenuServices
    {
        private readonly IMenuRepository repository;

        public MenuService(IMenuRepository menuRepository)
        {
            repository = menuRepository;
        }

        public async Task<IEnumerable<Application>> GetMenu() => await repository.GetMenu();

        public async Task<IEnumerable<Application>> UpdateMenu() => await repository.UpdateAsync(new Application[] { new Application { Description = "PRUEBA" } });
    }
}
