﻿using ValidationAttributeExtend;

namespace ValidationAttributeExtendTest
{
    public class ValidatorClass
    {
        [RequiredIf("PropertyTwo")]
        public string PropertyOne { get; set; }

        [RequiredIf("PropertyOne")]
        public string PropertyTwo { get; set; }
    }
}
