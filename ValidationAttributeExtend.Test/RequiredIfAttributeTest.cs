using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using ValidationAttributeExtend;

namespace ValidationAttributeExtendTest
{
    [TestFixture]
    public class RequiredIfAttributeTest
    {
        [TestCaseSource(typeof(RequiredIfAttributeTest), nameof(IsValidTestData))]
        public void IsValidTest(ValidatorClass validatorClass, bool expectedResult)
        {
            ValidationContext context = new ValidationContext(validatorClass, null, null);
            List<ValidationResult> results = new List<ValidationResult>();

            bool isValid = Validator.TryValidateObject(validatorClass, context, results, true);

            Assert.AreEqual(isValid, expectedResult);
            Assert.IsTrue(results.Count == 0);
        }

        [TestCaseSource(typeof(RequiredIfAttributeTest), nameof(NotIsValidTestData))]
        public void NotIsValidTest(ValidatorClass validatorClass, List<ValidationResult> expectedValidationResults, bool expectedResult)
        {
            ValidatorClassInvalid classInvalid = new ValidatorClassInvalid();
            ValidationContext context = new ValidationContext(classInvalid, null, null);
            List<ValidationResult> results = new List<ValidationResult>();

            bool isValid = Validator.TryValidateObject(classInvalid, context, results, true);

            Assert.AreEqual(isValid, expectedResult);
            CollectionAssert.AreEqual(
                expectedValidationResults.Select(x => x.ErrorMessage), 
                results.Select(x => x.ErrorMessage)
            );
        }

        [Test]
        public void IfValidationContextIsNullReturnArgumentNullException()
        {
            RequiredIfAttribute requiredIfAttribute = new RequiredIfAttribute("");

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(() => requiredIfAttribute.IsValid(""));

            Assert.IsTrue(exception.Message.Contains("Must specified a value to validationContext."));
        }

        [Test]
        public void IfOtherPropertyNotExistsReturnValidationResultError()
        {
            ValidatorClassInvalid classInvalid = new ValidatorClassInvalid();
            ValidationContext context = new ValidationContext(classInvalid, null, null);
            List<ValidationResult> results = new List<ValidationResult>();

            Validator.TryValidateObject(classInvalid, context, results, true);

            Assert.AreEqual("Could not find a property named PropertyNotExist.", results[0].ErrorMessage);
        }

        private static IEnumerable<TestCaseData> IsValidTestData {
            get {
                yield return new TestCaseData
                (
                    new ValidatorClass { PropertyOne = "SomeValue", PropertyTwo = null }, true
                )
                .SetName("Property one full and property two empty, return successfully");
                yield return new TestCaseData
                (
                    new ValidatorClass { PropertyOne = null, PropertyTwo = "SomeValue" }, true
                )
                .SetName("Property one empty and property two full, return successfully");
                yield return new TestCaseData
                (
                    new ValidatorClass { PropertyOne = "SomeValue", PropertyTwo = "SomeValue" }, true
                )
                .SetName("Property one full and property two full, return successfully");
            }
        }

        private static IEnumerable<TestCaseData> NotIsValidTestData {
            get {
                yield return new TestCaseData
                (
                    new ValidatorClass { PropertyOne = null, PropertyTwo = null }, 
                    new List<ValidationResult>
                    {
                        new ValidationResult("Could not find a property named PropertyNotExist."),
                        new ValidationResult("The property PropertyTwo is required because the property PropertyOne has not a value.")
                    },
                    false
                )
                .SetName("Properties are nulls, return validation result errors");
                yield return new TestCaseData
                (
                    new ValidatorClass { PropertyOne = string.Empty, PropertyTwo = string.Empty},
                    new List<ValidationResult>
                    {
                        new ValidationResult("Could not find a property named PropertyNotExist."),
                        new ValidationResult("The property PropertyTwo is required because the property PropertyOne has not a value.")
                    },
                    false
                )
                .SetName("Properties are string emptys, return validation result errors");
            }
        }
    }
}
