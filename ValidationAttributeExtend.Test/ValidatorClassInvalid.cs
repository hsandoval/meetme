﻿using ValidationAttributeExtend;

namespace ValidationAttributeExtendTest
{
    public class ValidatorClassInvalid
    {
        [RequiredIf("PropertyNotExist")]
        public string PropertyOne { get; set; }

        [RequiredIf("PropertyOne")]
        public string PropertyTwo { get; set; }
    }
}
